import Hapi from 'hapi';
import Boom from 'boom';
import dictionary from 'an-array-of-english-words';
var t9 = require('super-t9');

t9.setWordList('english', 'array', dictionary);

const server = new Hapi.Server();
const badData = Boom.badData('Only numerals 2-9 are allowed.');
server.connection({
    routes: { cors: true },
    host: 'localhost',
    port: 8000
});

server.route({
    method: 'GET',
    path: '/words/{number}',
    handler: (request, reply) => {
        const number = request.params.number;
        if (!/^[2-9]*$/.test(number)) {
            return reply(badData);
        }

        const predictions = t9.getWordsFromNumber(number, 'english');

        return predictions ? reply(JSON.stringify({words: predictions})) : reply(badData);
    }
});

server.start((err) => {
    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});
